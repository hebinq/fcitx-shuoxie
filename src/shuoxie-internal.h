/***************************************************************************
 *   Copyright (C) 2017 by hebinq                                          *
 *   hebinq@qq.com                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.              *
 ***************************************************************************/

#ifndef _FCITX_HANDWRITE_INTERNAL_H_
#define _FCITX_HANDWRITE_INTERNAL_H_

#include "shuoxie.h"
#include <fcitx/instance.h>
#include <dbus/dbus.h>
#define _(x) dgettext("fcitx-shuoxie", x)

typedef struct {
    FcitxGenericConfig gconfig;
 

} FcitxShuoXieConfig;

typedef struct {
    FcitxShuoXieConfig config;
    FcitxInstance *owner;    
} FcitxShuoXie;

CONFIG_BINDING_DECLARE(FcitxShuoXieConfig);

#endif
